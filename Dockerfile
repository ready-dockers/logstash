FROM "docker.elastic.co/logstash/logstash:6.4.1"
WORKDIR "/var/data"
COPY logstash.yml /usr/share/logstash/config/logstash.yml
ENTRYPOINT ["logstash","-f","push.conf"]

