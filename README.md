# logstash

Logstash and sample data with configuration to push to elastic-search

It will
- Connect to elastic-search at http://elasticsearch1:9200 (change data/push.conf file if required)
- Push 20 sample rows to elastic-search with predefined fields (change data/push.conf if required)

It must be
- Able to connect to elastic-search or make sure to setup proper network on docker-compose.yml if elastic-search is running on a docker container